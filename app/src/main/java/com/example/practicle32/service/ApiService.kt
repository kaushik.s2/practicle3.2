package com.example.practicle32.service

import com.example.practicle32.Users
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("users")
    fun getUserList(): Call<List<Users>>
}