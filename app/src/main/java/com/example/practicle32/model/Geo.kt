package com.example.practicle32.model

data class Geo(
    val lat:String,
    val lng:String,
)
