package com.example.practicle32

import com.example.practicle32.model.Address
import com.example.practicle32.model.Company

data class Users(
    val id:String,
    val name: String,
    val username:String,
    val email:String,
    val address: Address,
    val phone:String,
    val website:String,
    val company: Company
)





