package com.example.practicle32.model

data class Company(
    val name:String,
    val catchPhrase:String,
    val bs:String
)