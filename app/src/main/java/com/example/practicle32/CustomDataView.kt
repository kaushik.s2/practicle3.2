package com.example.practicle32

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.practicle32.service.ApiService
import com.example.practicle32.ui.theme.Practicle32Theme
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class CustomDataView : ComponentActivity() {
    lateinit var retrofit: Retrofit
    lateinit var service: ApiService
    val BASE_URL: String = "https://jsonplaceholder.typicode.com/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(ApiService::class.java)
        val call: Call<List<Users>> = service.getUserList()
        call.enqueue(object : Callback<List<Users>> {
            override fun onResponse(call: Call<List<Users>>, response: Response<List<Users>>) {
                if (response.isSuccessful) {
                    val res: ArrayList<Users> = (response.body() as ArrayList<Users>)

                    setContent {
                        Practicle32Theme {
                            // A surface container using the 'background' color from the theme
                            Surface(color = MaterialTheme.colors.background) {
                                val navController = rememberNavController()
                                NavHost(
                                    navController = navController,
                                    startDestination = "view_users",
                                    builder = {
                                        composable(
                                            "view_users",
                                            content = {
                                                UserListView(
                                                    res,
                                                    navController = navController
                                                )
                                            })
                                        composable(
                                            "remove_user",
                                            content = {
                                                RemoveUser(
                                                    res,
                                                    navController = navController
                                                )
                                            })
                                    })
                            }
                        }
                    }
                }
            }
            override fun onFailure(call: Call<List<Users>>, t: Throwable) {
                Toast.makeText(applicationContext, "" + t.message, Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun UserListView(usersList: ArrayList<Users>, navController: NavHostController) {
    val context = LocalContext.current
    LazyColumn {
        itemsIndexed(usersList) { index, item ->
            Card(
                modifier = Modifier
                    .padding(8.dp, 4.dp)
                    .fillMaxWidth()
                    .combinedClickable(
                        onClick = {
                        },
                        onLongClick = {
                            val builder = AlertDialog.Builder(context)
                            builder
                                .setMessage(
                                    "Do you want to remove " + usersList[index].name
                                            + " from list?"
                                )
                                .setCancelable(true)
                                .setPositiveButton("Yes") { _, _ ->
                                    usersList.removeAt(index)
                                    navController.navigate(route = "remove_user") {
                                        popUpTo(navController.graph.startDestinationId)
                                        launchSingleTop = true
                                    }
                                }
                                .setNegativeButton(
                                    "No"
                                ) { _, _ -> }
                            val alertDialog = builder.create()
                            alertDialog.setTitle("")
                            alertDialog.show()
                        }
                    ),
                shape = RoundedCornerShape(8.dp),
                elevation = 4.dp,
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Row(Modifier.padding(4.dp)) {
                        Text(
                            text = stringResource(R.string.name),
                            color = colorResource(id = R.color.black)
                        )
                        (Text(
                            text = item.name, Modifier.padding(start = 8.dp), maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        ))
                    }
                    Row(Modifier.padding(4.dp)) {
                        Text(
                            text = stringResource(R.string.mail_id),
                            color = colorResource(id = R.color.black)
                        )
                        (Text(
                            text = item.email, Modifier.padding(start = 12.dp),
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        ))
                    }
                    Row(Modifier.padding(4.dp)) {
                        Text(
                            text = stringResource(R.string.city),
                            color = colorResource(id = R.color.black)
                        )
                        (Text(
                            text = item.address.city, Modifier.padding(start = 16.dp), maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        ))
                    }
                }
            }
        }
    }
}

@Composable
private fun RemoveUser(usersList: ArrayList<Users>, navController: NavHostController) {
    UserListView(usersList = usersList, navController = navController)
}

